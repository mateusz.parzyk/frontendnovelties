assignmentObject = {
  a: 'existing attribute',
  b: '',
  c: null
}
chainingObject = {
  a: 'existing atribute',
  existingFunction: () => {
    console.log('halo');
  }
}

function testOR(entry) {
  const result = entry || 'default string'
  console.log(result);
}

function testNullish(entry) {
  const result = entry ?? 'default string'
  console.log(result);
}

function testNullishAssignment() {
  assignmentObject.a ??= 'some new text';
  assignmentObject.b ??= 'some new text';
  assignmentObject.c ??= 'some new text';
  console.log(assignmentObject);
}

function testLogicalANDAssignment() {
  assignmentObject.a &&= 'some new text';
  assignmentObject.b &&= 'some new text';
  assignmentObject.c &&= 'some new text';
  console.log(assignmentObject);
}
function testLogicalORAssignment() {
  assignmentObject.a ||= 'some new text';
  assignmentObject.b ||= 'some new text';
  assignmentObject.c ||= 'some new text';
  console.log(assignmentObject);
}

function noOptionalChaining(entry) {
  // console.log(entry.b.c);
  entry.existingFunction();
  entry.nonexistingFunction();
}
function optionalChaining(entry) {
  // console.log(entry.b?.c);
  entry.existingFunction?.();
  entry.nonexistingFunction?.();
}

async function dynamicImport() {
  const importedModule = await import('./someFunction.js');
  importedModule.someFunction();
}